﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BioButtons : MonoBehaviour {

	public Text HaggeText;
	public Text JavisText;
	public Text SerthisText;

	public void DisplayText() {
		HaggeText.text = "Hagge loves everything.";
		JavisText.text = "Brought in as requested by his parents after their death, Javis only knows the order to be his family.";
		SerthisText.text = "He's Death ok? Just be scared.";
	}

	// Use this for initialization
	void Start () {
		HaggeText.text = "";
		JavisText.text = "";
		SerthisText.text = "";
	}

	// Update is called once per frame
	void Update () {
		
	}
}
